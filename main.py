from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from utils.annote import get_entity_list
from utils.calc_triple import get_all_relations

class MsgText(BaseModel):
    text:str

app = FastAPI()
origins = [
    "http://localhost.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:4200",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.post("/extract")
async def extract(msgText:MsgText):
    print("analyses text")
    print(msgText.text[:100])
    entities = get_entity_list(msgText.text)
    relations = get_all_relations(entities)

    print(f"found {len(entities)} entities.")
    return{"entities":entities,
    "relations": relations}