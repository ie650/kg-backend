import requests
import json
from typing import Any, List, Dict, Tuple
import pandas as pd
import numpy as np
from SPARQLWrapper import SPARQLWrapper, JSON
import itertools

def _collect_URIs_pairwise(df:pd.DataFrame):
    pair_order_list = itertools.combinations(df['URI'].unique(),2)
    return list(pair_order_list)

def _strip_url(st):
    return st.split("http://dbpedia.org/resource/",1)[1]

def _query(sparql):
    l_res = []
    try:
        ret = sparql.queryAndConvert()
        for r in ret["results"]["bindings"]:
            res = r['x']['value']
            l_res.append(res)
            #print(res)
        return(l_res)
    except Exception as e:
        print(e)

def _generate_Relations(df)->List[Dict[str, Any]] :
    relations:List[Dict[str, Any]] = []
    pairs = _collect_URIs_pairwise(df)
    for p in pairs:
        w1 = _strip_url(p[0])
        if "." in w1:
            w1 = w1.replace('.','')
        w2 = _strip_url(p[1])
        if "." in w2:
            w2 = w2.replace('.','')


        sparql = _primary_relations(w1, w2)
        l = _query(sparql)
        
        sparql = _secondary_relations(w1, w2)
        l2 = _query(sparql)
        #l = _query(sparql)
        #sparql = _secondary_relations(w1, w2)
        #l2 = _query(sparql)
        #l = list(np.unique(l+l2))
        l2 =list(np.unique(l2))
        if l == None:
            l =[]
        if l2 == None:
            l2 = []

        if "http://dbpedia.org/ontology/wikiPageWikiLink" in l:
            l.remove("http://dbpedia.org/ontology/wikiPageWikiLink")
        if "http://dbpedia.org/ontology/wikiPageRedirects" in l:
            l.remove("http://dbpedia.org/ontology/wikiPageRedirects")

        if "http://dbpedia.org/ontology/wikiPageWikiLink" in l2:
            l2.remove("http://dbpedia.org/ontology/wikiPageWikiLink")
        if "http://dbpedia.org/ontology/wikiPageRedirects" in l2:
            l2.remove("http://dbpedia.org/ontology/wikiPageRedirects")
        if None in l2:
            l2.remove(None)
        if "None" in l2:
            l2.remove("None")
        

   
        for entry in l:
            if l2 != None and entry in l2:
                l2 = l2.remove(entry)


        if len(l) > 0 or len(l2)>0:
            relations.append({
                "from":w1,
                "to":w2,
                "primary":l,
                "secoundary":l2})
            print('relations: ' + w1 + ' -> ' + w2)
            print(l)
            print(l2)
    return relations

def _primary_relations(w1, w2):
    sparql = SPARQLWrapper("https://dbpedia.org/sparql/")
    sparql.setReturnFormat(JSON)
    q = f"""
        PREFIX dbr:  <http://dbpedia.org/resource/>
        SELECT ?x
        WHERE {{
            dbr:{w1} ?x dbr:{w2} .
        }}
        LIMIT 50
        """
    sparql.setQuery(q)
    return sparql

def _secondary_relations(w1, w2):
    sparql = SPARQLWrapper("https://dbpedia.org/sparql/")
    sparql.setReturnFormat(JSON)
    q = f"""
        PREFIX dbr:  <http://dbpedia.org/resource/>
        SELECT ?x
        WHERE {{
            dbr:{w1} ?y ?z .
            ?z ?x dbr:{w2} .
        }}
        LIMIT 50
        """
    sparql.setQuery(q)
    return sparql

def get_all_relations(candidates: List[str]) -> List[Dict[str, Any]] :
    data = pd.json_normalize(candidates)
    return _generate_Relations(data)

