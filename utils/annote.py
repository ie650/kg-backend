import requests
import json
from typing import Any, List, Dict

# tests with 7410 chars and did not get an error
# swagger UI: https://www.dbpedia-spotlight.org/api
def get_entity_list(text: str, confidence: float = 0.5) -> List[str]:
    """Generate list containing the URIs and information of the named entities recognised in the text provided."""
    if confidence > 1 or confidence < 0:
        raise ValueError('Confidence has to be in the interval [0,1].')

    params = {
        'text': text,
        'confidence' : confidence
    }
    headers = {
        'accept': 'application/json'
    }
    resp = requests.get('https://api.dbpedia-spotlight.org/en/annotate', params=params, headers=headers)

    if resp.status_code != 200:
        raise ValueError('The provided text resulted in an API error: {errorcode}'.format(errorcode=resp.status_code))
    return _remove_at_in_keys(_extract_used_information(json.loads(resp.content)['Resources']))


def _extract_used_information(named_entity_entry: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    """Extracts the needed information for each found named entity used in the UI."""
    # TODO add potential selection of wanted keys
    return named_entity_entry

def _remove_at_in_keys(named_entity_entry: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    updated_named_entity_entry : List[Dict[str, Any]] = []
    for entity in named_entity_entry:
        updated_entity:Dict[str, Any] = {}
        for key in entity.keys():
            if key[0] == "@":
                updated_entity[key[1:]] = entity[key]
            else:
                updated_entity[key] = entity[key]
        updated_named_entity_entry.append(updated_entity)
    return updated_named_entity_entry
